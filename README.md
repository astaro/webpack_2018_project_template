# About

7/18/2018: This is about where I need it to for creating a new, updated build system for things like the sinlge page apps I was doing for SciencePrimer a few years ago. 

Some additional modifications and customizations would be needed but this underlyning build system is sufficent and current (as of summer 2018) to work as the foundation. 

Update (redo) of a webpack-based build system using the most recent version of the [SurviveJS webpack book](https://survivejs.com/webpack/) as of summer 2018. 

Aiming for something like my old [single page builder]( app build system from (https://github.com/AndrewStaroscik/webpack-single-page-builder) from 2016

The goal for this build system is to have a quick way to set up new builds for small single page apps as such it does not need all of the bells and whistles that are so important/helpful for larger apps. This system does no code splitting no buldle splitting and does not add hashes to the filenames.