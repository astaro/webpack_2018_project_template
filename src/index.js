import './main.styl';

import gL from './gL/gL';
import gLInit from './gL/gLInit';

import component from './helpers/component';
import svgTest from './helpers/svgTest';



gLInit(gL);

gL.mainF.appendChild(component());
gL.mainF.appendChild(svgTest());