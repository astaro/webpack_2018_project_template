// this works for jpg, gif etc also. 
// this use of svg is as an embedded image, setAttributeNS(...) etc is needed to generate svg as with D3n

import testSVG from '../imgs/SVG/svg-1.svg';

export default (text = "webpack project template with hot reload") => {
  const element = document.createElement("img");
  

  element.setAttribute('src',testSVG);
  element.setAttribute('width', '100px');
  element.setAttribute('height', '100px');
  element.setAttribute('id', "svg_img");

 
  return element;
}