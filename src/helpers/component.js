export default (text = "webpack project template with hot reload") => {
  const element = document.createElement("div");

  element.innerHTML = text;
  element.classList.add('typicaldiv');

  return element;
}