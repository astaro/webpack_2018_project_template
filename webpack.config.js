const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const path = require("path");
const glob = require("glob");

const PATHS = {
  app: path.join(__dirname, 'src'),
  build: path.join(__dirname, 'dist'),
};

const parts = require("./wpParts/webpack.parts");

const commonConfig = merge([
  {
    optimization: {
      minimize: true, // this is the default behavior. Set to false to see code
    }
  },
  {
    plugins: [
      new HtmlWebpackPlugin({
        template: './wpParts/index.ejs',
        title: "single page app demo",
        author: 'Andrew Staroscik'
      }),
    ],
  }, 
  parts.loadSVGs({
    options: {
      name: "svg/[name].[ext]",
    }
  }),
  //parts.loadCSS(),
  //parts.loadStylus(),
]);

const productionConfig = merge([
  //parts.loadStylus(),
  //parts.generateSourceMaps({ type: "source-map" }), // enable to get surce maps and consider "nosource-source-map" option
  parts.clean(PATHS.build),
  parts.extractCSS({
    use: [
      "css-loader",
      parts.autoprefix(),
      {
        loader: "stylus-loader",
      },
    ],
  }),



  
  parts.loadImages({
    options: { 
      //limit: 15000,
      limit: 1,
      name: "imgs/[name].[ext]",
    },
  }),


  // ***** disabling purifyCSS because it is removing classes added by js and I don't want to deal with it now //
  // parts.purifyCSS({
  //   paths: glob.sync('${PATHS.app}/**/*.js',{nodir: true}),
    
  // }),
  parts.loadJavaScript({ exclude: path.resolve(__dirname, '/node_modules') }),


]);

const developmentConfig = merge([
  parts.generateSourceMaps({ type: "eval-source-map" }),

  parts.devServer({
    // Customize host/port here if needed
    host: '0.0.0.0',
    port: process.env.PORT,
  }),
  parts.loadStylus(),
  parts.loadImages(),
]);

module.exports = mode => {
  if (mode === "production") {
    return merge(commonConfig, productionConfig, { mode });
  }

  return merge(commonConfig, developmentConfig, { mode });
};
